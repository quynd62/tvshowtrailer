package lab02.n0804.tvshowtrailer.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import lab02.n0804.tvshowtrailer.R;
import lab02.n0804.tvshowtrailer.activity.TvShowDetailActivity;
import lab02.n0804.tvshowtrailer.model.ApiResponseShow;
import lab02.n0804.tvshowtrailer.model.ModelTvShowTest;
import lab02.n0804.tvshowtrailer.room.DBConnection;
import lab02.n0804.tvshowtrailer.room.TvShowFavoriteDAO;

public class TvShowAdapter extends RecyclerView.Adapter<TvShowAdapter.ViewHolder>{
    Context context;
    ArrayList<ApiResponseShow.Show> arrTvShow;

    public TvShowAdapter(Context context, ArrayList<ApiResponseShow.Show> arrTvShow) {
        this.context = context;
        this.arrTvShow = arrTvShow;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_tvshow,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ApiResponseShow.Show apiResponseShow = arrTvShow.get(position);
        String urlPoster = "https://image.tmdb.org/t/p/w500"+apiResponseShow.getPosterPath();
        Picasso.with(context).load(urlPoster).into(holder.imgTvShow);
        //holder.imgTvShow.setImageResource(apiResponseShow.getImgTvShow());
        holder.tvNameofTvShow.setText(apiResponseShow.getOriginalName());
        holder.tvDetail.setText(apiResponseShow.getFirstAirDate());
        holder.imgTvShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, ""+apiResponseShow.getId(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, TvShowDetailActivity.class);
                intent.putExtra("idTvShow",apiResponseShow.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrTvShow.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imgTvShow;
        TextView tvNameofTvShow,tvDetail;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgTvShow = itemView.findViewById(R.id.imgTvShow);
            tvNameofTvShow = itemView.findViewById(R.id.tvNameofTvShow);
            tvDetail = itemView.findViewById(R.id.tvDetail);
        }
    }
}
