package lab02.n0804.tvshowtrailer.model;

public class ModelTvShowTest {
    private int imgTvShow;
    private String nameOfTvShow;
    private String detailTvShow;

    public ModelTvShowTest(int imgTvShow, String nameOfTvShow, String detailTvShow) {
        this.imgTvShow = imgTvShow;
        this.nameOfTvShow = nameOfTvShow;
        this.detailTvShow = detailTvShow;
    }

    public ModelTvShowTest() {
    }

    public int getImgTvShow() {
        return imgTvShow;
    }

    public void setImgTvShow(int imgTvShow) {
        this.imgTvShow = imgTvShow;
    }

    public String getNameOfTvShow() {
        return nameOfTvShow;
    }

    public void setNameOfTvShow(String nameOfTvShow) {
        this.nameOfTvShow = nameOfTvShow;
    }

    public String getDetailTvShow() {
        return detailTvShow;
    }

    public void setDetailTvShow(String detailTvShow) {
        this.detailTvShow = detailTvShow;
    }

    @Override
    public String toString() {
        return "ModelTvShowTest{" +
                "imgTvShow=" + imgTvShow +
                ", nameOfTvShow='" + nameOfTvShow + '\'' +
                ", detailTvShow='" + detailTvShow + '\'' +
                '}';
    }
}
