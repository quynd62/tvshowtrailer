package lab02.n0804.tvshowtrailer.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import lab02.n0804.tvshowtrailer.R;
import lab02.n0804.tvshowtrailer.model.ApiResponseMovieVideo;
import lab02.n0804.tvshowtrailer.model.ApiResponseShowDetail;
import lab02.n0804.tvshowtrailer.model.ApiResponseShowVideo;
import lab02.n0804.tvshowtrailer.room.DBConnection;
import lab02.n0804.tvshowtrailer.room.TvShowFavorite;
import lab02.n0804.tvshowtrailer.room.TvShowFavoriteDAO;
import lab02.n0804.tvshowtrailer.room.TvShowViewModel;
import lab02.n0804.tvshowtrailer.service.ConnectServer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvShowDetailActivity extends AppCompatActivity {
    CoordinatorLayout coordinatorLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Toolbar toolbar;
    FloatingActionButton floatingActionButton;
    CardView cardViewTrailer;
    int idTvShow;
    String nameTvShow = "";
    String urlPoster = "";
    ImageView imgPoster;
    ArrayList<ApiResponseShowDetail.Genres> listGenre = new ArrayList<>();
    DBConnection dbConnection;
    TvShowFavoriteDAO tvShowFavoriteDAO;
    String keyVideo = "";
    String dateShow = "";
    TvShowViewModel tvShowViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_show_detail);
        tvShowViewModel = new ViewModelProviders().of(this).get(TvShowViewModel.class);
        dbConnection = Room.databaseBuilder(getApplicationContext(),DBConnection.class,"database.db").build();
        mapping();
        getDataIntent();
        init();
        callApiGetVideo();
        callApiGetDetailTvShow();
    }

    private void callApiGetVideo() {
        ConnectServer.getApiService().getListShowVideo(idTvShow).enqueue(new Callback<ApiResponseMovieVideo>() {
            @Override
            public void onResponse(Call<ApiResponseMovieVideo> call, Response<ApiResponseMovieVideo> response) {
                if (response.isSuccessful()){
                    ApiResponseMovieVideo apiResponseMovieVideoo = response.body();
                    //Log.d("Video", " "+response.toString());
                    keyVideo = apiResponseMovieVideoo.getResults().get(0).getKey();
                    Log.d("keyVideo", " "+keyVideo);
                    eventClickPlay();
                }
            }

            @Override
            public void onFailure(Call<ApiResponseMovieVideo> call, Throwable t) {

            }
        });

    }

    private void init() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        collapsingToolbarLayout.setExpandedTitleColor(Color.WHITE);
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.WHITE);

    }

    private void getDataIntent() {
        Intent intent = getIntent();
        if (intent != null){
            idTvShow = intent.getIntExtra("idTvShow",10);
            floatingActionButton.setImageResource(tvShowViewModel.isFavorite(idTvShow) ? R.drawable.iconloved : R.drawable.iconlove);
        }
    }

    private void mapping() {
        coordinatorLayout = findViewById(R.id.coordinatorlayout);
        collapsingToolbarLayout = findViewById(R.id.collapsingtoolbar);
        toolbar = findViewById(R.id.toolbar);
        floatingActionButton = findViewById(R.id.floatingactionbutton);
        cardViewTrailer = findViewById(R.id.cardviewTrailer);
        imgPoster = findViewById(R.id.imgPoster);

//        dbConnection = Room.databaseBuilder(this,DBConnection.class,"mydata.db")
//                    .allowMainThreadQueries()
//                    .build();
//        tvShowFavoriteDAO = dbConnection.createTvShowFavDAO();
    }

    private void callApiGetDetailTvShow() {
        ConnectServer.getApiService().getDetailShow(idTvShow).enqueue(new Callback<ApiResponseShowDetail>() {
            @Override
            public void onResponse(Call<ApiResponseShowDetail> call, Response<ApiResponseShowDetail> response) {
                if (response.isSuccessful()) {
                    Log.d("detail", " "+response.toString());
                    ApiResponseShowDetail apiResponseShowDetail = response.body();
                    nameTvShow = apiResponseShowDetail.getName();
                    urlPoster = "https://image.tmdb.org/t/p/w500"+apiResponseShowDetail.getBackdropPath();
                    dateShow = apiResponseShowDetail.getFirstAirDate();
                    setValuesInView();

                    eventClick();
                    //listGenre.addAll(apiResponseShowDetail.getGenres());
                }
            }

            @Override
            public void onFailure(Call<ApiResponseShowDetail> call, Throwable t) {

            }
        });
    }

    private void setValuesInView() {
        collapsingToolbarLayout.setTitle(nameTvShow);
        Picasso.with(this).load(urlPoster).into(imgPoster);
    }

    private void eventClick(){
        TvShowFavorite tvShowFavorite2 = new TvShowFavorite(String.valueOf(idTvShow),nameTvShow,dateShow,urlPoster);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvShowViewModel.isFavorite(idTvShow)){
                    floatingActionButton.setImageResource(R.drawable.iconlove);
                    tvShowViewModel.DeteleShowFav(tvShowFavorite2);
                    Toast.makeText(TvShowDetailActivity.this, "Delete Success", Toast.LENGTH_SHORT).show();
                }
                else{
                    floatingActionButton.setImageResource(R.drawable.iconloved);
                    tvShowViewModel.AddTvShowFav(tvShowFavorite2);
                    Toast.makeText(TvShowDetailActivity.this, "Add Success", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void eventClickPlay(){
        cardViewTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(TvShowDetailActivity.this,PlayVideoActivity.class);
                intent2.putExtra("keyVideo",keyVideo);
                startActivity(intent2);
            }
        });

    }

}