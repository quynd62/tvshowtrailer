package lab02.n0804.tvshowtrailer.room;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class TvShowViewModel extends AndroidViewModel {
    private TvShowRepository tvShowRepository;
    private DBConnection db;
    private LiveData<List<TvShowFavorite>> tvShows;
    private TvShowFavorite tvShowFavorite;
    public TvShowViewModel(@NonNull Application application) {
        super(application);
        tvShowRepository = new TvShowRepository(application);
        tvShows = tvShowRepository.getAllShowFav();
        db = DBConnection.getDataBase(application);
    }

    public void AddTvShowFav(TvShowFavorite tvShowFavorite){
        tvShowRepository.addTvShowFav(tvShowFavorite);
    }
    public void DeteleShowFav(TvShowFavorite tvShowFavorite){
        tvShowRepository.deleteTvShowFav(tvShowFavorite);
    }

    public boolean isFavorite(int tvShowId){
        return tvShowRepository.getTvShowById(tvShowId) != null;
    }
    public LiveData<List<TvShowFavorite>> getAllShowFav(){
        return tvShowRepository.getAllShowFav();
    }
}
