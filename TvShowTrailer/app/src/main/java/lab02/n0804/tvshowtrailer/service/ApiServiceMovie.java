package lab02.n0804.tvshowtrailer.service;

import lab02.n0804.tvshowtrailer.model.ApiResponseMovieVideo;
import lab02.n0804.tvshowtrailer.model.ApiResponseShow;
import lab02.n0804.tvshowtrailer.model.ApiResponseShowDetail;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiServiceMovie {
    @GET("tv/popular?api_key=5fdde824d28c655234d04a2b334c783d")
    Call<ApiResponseShow> getListShowPopular();

    @GET("tv/{tv_id}?api_key=5fdde824d28c655234d04a2b334c783d")
    Call<ApiResponseShowDetail> getDetailShow(@Path("tv_id")int tv_id);

    @GET("tv/{tv_id}/videos?api_key=5fdde824d28c655234d04a2b334c783d")
    Call<ApiResponseMovieVideo> getListShowVideo(@Path("tv_id") int tv_id);
}
