package lab02.n0804.tvshowtrailer.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import lab02.n0804.tvshowtrailer.R;
import lab02.n0804.tvshowtrailer.adapter.MainViewPagerAdapter;
import lab02.n0804.tvshowtrailer.fragment.FavoriteFragment;
import lab02.n0804.tvshowtrailer.fragment.TvShowFragment;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mapping();
        init();
    }

    private void init() {
        MainViewPagerAdapter adapter = new MainViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TvShowFragment(),"TvShow");
        adapter.addFragment(new FavoriteFragment(),"Favorite");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_tvshow);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_heart);

    }

    private void mapping() {
        viewPager = findViewById(R.id.viewpager_main);
        tabLayout = findViewById(R.id.tablayout_main);
    }
}