package lab02.n0804.tvshowtrailer.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.ListViewCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lab02.n0804.tvshowtrailer.R;
import lab02.n0804.tvshowtrailer.adapter.FavoriteAdapter;
import lab02.n0804.tvshowtrailer.adapter.TvShowAdapter;
import lab02.n0804.tvshowtrailer.model.ApiResponseShow;
import lab02.n0804.tvshowtrailer.room.DBConnection;
import lab02.n0804.tvshowtrailer.room.TvShowFavorite;
import lab02.n0804.tvshowtrailer.room.TvShowFavoriteDAO;
import lab02.n0804.tvshowtrailer.room.TvShowViewModel;
import lab02.n0804.tvshowtrailer.service.ConnectServer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoriteFragment extends Fragment {
    View view;
    RecyclerView recyclerViewFavorite;
    FavoriteAdapter favoriteAdapter;
    ArrayList<ApiResponseShow.Show> listTvShow = new ArrayList<>();
    DBConnection dbConnection;
    TvShowFavoriteDAO tvShowFavoriteDAO;
    List<TvShowFavorite> list;
    TvShowViewModel tvShowViewModel;
    ConstraintLayout layout;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favorite,container,false);
        recyclerViewFavorite = view.findViewById(R.id.reycleviewFavorite);
        tvShowViewModel = new ViewModelProvider(this,new ViewModelProvider.AndroidViewModelFactory(getActivity().getApplication())).get(TvShowViewModel.class);
        list = new ArrayList<>();
        tvShowViewModel.getAllShowFav().observe(getViewLifecycleOwner(), new Observer<List<TvShowFavorite>>() {
            @Override
            public void onChanged(List<TvShowFavorite> tvShowFavorites) {
                list.clear();
                list.addAll(tvShowFavorites);
                favoriteAdapter = new FavoriteAdapter(getActivity(),list);
                recyclerViewFavorite.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerViewFavorite.setAdapter(favoriteAdapter);
            }
        });

        swipe();
        return view;
    }

    private void swipe() {

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP|ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            //ham onMove xử lý code drag and drop
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder dragged, @NonNull RecyclerView.ViewHolder target) {
                int positionDragged = dragged.getAdapterPosition();
                int positioTarget = target.getAdapterPosition();
                Collections.swap(list,positionDragged,positioTarget);
                favoriteAdapter.notifyItemMoved(positionDragged,positioTarget);
                return false;
            }

            @Override
            // hàm onSwipe xử  lý code trượt xóa
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                final TvShowFavorite detail = list.get(viewHolder.getAdapterPosition());
                final int deletedIndex = viewHolder.getAdapterPosition();
                tvShowViewModel.DeteleShowFav(favoriteAdapter.getMovieAt(viewHolder.getAdapterPosition()));
                favoriteAdapter.notifyDataSetChanged();
                Snackbar snackbar = Snackbar.make(layout, " Deleted from Favorite list", Snackbar.LENGTH_SHORT);
                snackbar.setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tvShowViewModel.AddTvShowFav(detail);
                        favoriteAdapter.notifyDataSetChanged();
                    }
                });
                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();
            }
        });
        itemTouchHelper.attachToRecyclerView(recyclerViewFavorite);
    }
}
