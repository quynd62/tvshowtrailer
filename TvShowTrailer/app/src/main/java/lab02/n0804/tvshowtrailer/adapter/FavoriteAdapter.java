package lab02.n0804.tvshowtrailer.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import lab02.n0804.tvshowtrailer.R;
import lab02.n0804.tvshowtrailer.activity.PlayVideoActivity;
import lab02.n0804.tvshowtrailer.activity.TvShowDetailActivity;
import lab02.n0804.tvshowtrailer.room.TvShowFavorite;
import lab02.n0804.tvshowtrailer.room.TvShowViewModel;


public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder>{
    Context context;
    List<TvShowFavorite> tvShowFavorites;
    TvShowViewModel tvShowViewModel;

    public FavoriteAdapter(Context context, List<TvShowFavorite> tvShowFavorites) {
        this.context = context;
        this.tvShowFavorites = tvShowFavorites;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_tvshow_favorite,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TvShowFavorite tvShowFavorite = tvShowFavorites.get(position);
        Picasso.with(context).load(tvShowFavorite.getUrlImg()).into(holder.imgTvShow);
        holder.tvNameofTvShow.setText(tvShowFavorite.getNameShow());
        holder.tvDetail.setText(tvShowFavorite.getDate());
        holder.imgTvShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TvShowDetailActivity.class);
                intent.putExtra("idTvShow",Integer.parseInt(tvShowFavorites.get(position).getIdTvShow()));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tvShowFavorites.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imgTvShow;
        TextView tvNameofTvShow,tvDetail;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgTvShow = itemView.findViewById(R.id.imgTvShow);
            tvNameofTvShow = itemView.findViewById(R.id.tvNameofTvShow);
            tvDetail = itemView.findViewById(R.id.tvDetail);

        }
    }

    public TvShowFavorite getMovieAt(int position){
        return tvShowFavorites.get(position);
    }
}
