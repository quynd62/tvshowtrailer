package lab02.n0804.tvshowtrailer.room;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class TvShowFavorite {
    @NonNull
    @PrimaryKey
    private String idTvShow;

    @ColumnInfo
    private String nameShow;

    @ColumnInfo
    private String date;

    @ColumnInfo
    private String urlImg;

    public TvShowFavorite() {
    }

    public TvShowFavorite(String idTvShow, String nameShow, String date, String urlImg) {
        this.idTvShow = idTvShow;
        this.nameShow = nameShow;
        this.date = date;
        this.urlImg = urlImg;
    }


    public String getNameShow() {
        return nameShow;
    }

    public void setNameShow(String nameShow) {
        this.nameShow = nameShow;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getIdTvShow() {
        return idTvShow;
    }

    public void setIdTvShow(String idTvShow) {
        this.idTvShow = idTvShow;
    }
}
