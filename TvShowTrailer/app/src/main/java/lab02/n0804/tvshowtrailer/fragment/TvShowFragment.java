package lab02.n0804.tvshowtrailer.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import lab02.n0804.tvshowtrailer.R;
import lab02.n0804.tvshowtrailer.adapter.TvShowAdapter;
import lab02.n0804.tvshowtrailer.model.ApiResponseShow;
import lab02.n0804.tvshowtrailer.model.ModelTvShowTest;
import lab02.n0804.tvshowtrailer.service.ConnectServer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvShowFragment extends Fragment {
    View view;
    RecyclerView recyclerViewTvShow;
    TvShowAdapter tvShowAdapter;
    ArrayList<ApiResponseShow.Show> listPopular = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tvshow, container, false);
        recyclerViewTvShow = view.findViewById(R.id.rcvTvShow);
        callApiShowPopular();
        return view;
    }

    private void callApiShowPopular() {
        ConnectServer.getApiService().getListShowPopular().enqueue(new Callback<ApiResponseShow>() {
            @Override
            public void onResponse(Call<ApiResponseShow> call, Response<ApiResponseShow> response) {
                if (response.isSuccessful()) {
                    ApiResponseShow apiResponseShow = response.body();
                    listPopular.addAll(apiResponseShow.getResults());
                    tvShowAdapter = new TvShowAdapter(getActivity(),listPopular);
                    recyclerViewTvShow.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                    recyclerViewTvShow.setAdapter(tvShowAdapter);
//                    initgetLimitSlide();
//                    listCategory.add(new CategoryShow("Popular", listPopular));
//                    adapterShowCategory.notifyDataSetChanged();
//                    adapterShowSlide.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ApiResponseShow> call, Throwable t) {

            }
        });
    }
}
